import requests
import sys

def get_word_definition(api_key, word):
    url = f"https://www.dictionaryapi.com/api/v3/references/collegiate/json/{word}?key={api_key}"
    
    try:
        response = requests.get(url)
        response.raise_for_status()
        data = response.json()

        if isinstance(data, list):
            # The API may return multiple results for a word. We use the first one.
            first_result = data[0]
            if isinstance(first_result, dict) and 'shortdef' in first_result:
                return first_result['shortdef']
            else:
                return None
        else:
            return None
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")
        return None

def format_definition(word, definitions):
    if not definitions:
        return f"Definition not found for '{word}'."

    formatted_definitions = "\n".join([f"{index + 1}. {definition}" for index, definition in enumerate(definitions)])
    return f"{word.capitalize()}:\n{formatted_definitions}"

def main(api_key, word):
    definitions = get_word_definition(api_key, word)

    if definitions:
        formatted_output = format_definition(word, definitions)
        print(formatted_output)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python dictionary_tool.py <API_KEY> <WORD>")
        sys.exit(1)

    api_key = sys.argv[1]
    word = sys.argv[2]

    main(api_key, word)
